<html lang="en" class="js">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<meta name="keywords" content="" />
	<meta name="author" content="Ade" />
	<link rel="shortcut icon" href="favicon.png">
	<link rel="stylesheet" type="text/css" href="css/base.css" />
	<link rel="stylesheet" type="text/css" href="css/discon-login.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>
<body>
	<div class="master-body">
		<div class="logo">
			<img src="images/logo-white.png" alt="">
		</div>
		<div class="upper">
			<div class="title">Sign In</div>
			<div class="subtitle">Please sign to continue using our app</div>
		</div>
		<div class="form-wp">
			<form action="#" autocomplete="on">
				<div class="form-control">
					<span><i class="fas fa-user"></i></span>
					<input type="text" placeholder="Username" autocomplete="off" value="">
				</div>
				<div class="form-control">
					<span><i class="fas fa-lock"></i></span>
					<input type="password" placeholder="Password" autocomplete="off" value="">
				</div>
				<div class="form-control">
					<button>Sign In</button>
				</div>
			</form>
		</div>
		<div class="bottom-image">
			<img src="images/header-background.png" alt="">
		</div>
	</div>
</body>
</html>